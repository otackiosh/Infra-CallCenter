package com.claro.DesenvolvimentodeSoftwareInfraCallCenter.repository;

import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.entity.ContatoEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ContatoRepository extends PagingAndSortingRepository<ContatoEntity, Long>, JpaSpecificationExecutor<ContatoEntity> {
    Page<ContatoEntity> findAll(Specification spec, Pageable pageable);
    Optional<ContatoEntity> findByEmail(String email);
    void deleteByEmail(String email);
}
