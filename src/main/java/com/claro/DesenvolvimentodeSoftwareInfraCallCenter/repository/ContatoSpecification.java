package com.claro.DesenvolvimentodeSoftwareInfraCallCenter.repository;

import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.dto.ContatoFilterDTO;
import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.entity.ContatoEntity;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Component
public class ContatoSpecification {

    private Specification<ContatoEntity> createSpecificationFilters(ContatoFilterDTO filter) {
        switch (filter.getOperator()) {
            case LIKE:
                return (root, query, criteriaBuilder) -> criteriaBuilder.like(root.get(filter.getFieldName()), "%" + filter.getValue() + "%");
            case EQUALS:
                return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(filter.getFieldName()), filter.getValue());
            case NOT_EQUALS:
                return (root, query, criteriaBuilder) -> criteriaBuilder.notEqual(root.get(filter.getFieldName()), filter.getValue());
            case GREATER_EQUAL_THAN:
                return (root, query, criteriaBuilder) -> criteriaBuilder.greaterThanOrEqualTo(root.get(filter.getFieldName()), (Date) castToDateType(filter.getValue()));
            case LESS_EQUAL_THAN:
                return (root, query, criteriaBuilder) -> criteriaBuilder.lessThanOrEqualTo(root.get(filter.getFieldName()), (Date) castToDateType(filter.getValue()));

            default:
                throw new RuntimeException("Operador nao suportado");
        }
    }

    public Specification<ContatoEntity> getSpecificationsFromFilter(List<ContatoFilterDTO> filters) {
        if (filters.isEmpty() || Objects.isNull(filters)) {
            return null;
        }

        Specification<ContatoEntity> specification = Specification.where(createSpecificationFilters(filters.remove(0)));
        for (ContatoFilterDTO filter : filters) {
            specification = specification.and(createSpecificationFilters(filter));
        }

        return specification;
    }


    private Object castToDateType(String value) {
        try {
            return new SimpleDateFormat("dd/MM/yyyy").parse(value);
        } catch (ParseException p) {
            throw new RuntimeException("Data invalida: " + p);
        }
    }
}
