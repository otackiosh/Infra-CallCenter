package com.claro.DesenvolvimentodeSoftwareInfraCallCenter.dao;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.text.SimpleDateFormat;
import java.util.Date;

@Getter @Setter @AllArgsConstructor @NoArgsConstructor
public class ContatoDAO {
    private String email;
    private String name;
    private String telephone;
    private String cep;
    private String address;
    private String city;
    private String uf;
    private String dateOfCreation;

    public void setDateOfCreation(Date dateOfCreation) {
        this.dateOfCreation = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(dateOfCreation);
    }
}
