package com.claro.DesenvolvimentodeSoftwareInfraCallCenter.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "tb_contato")
@Builder @AllArgsConstructor @Getter @Setter @NoArgsConstructor
public class ContatoEntity {

    @Id
    private String email;
    @Column(name = "nome")
    private String name;
    @Column(name = "telefone")
    private String telephone;
    private String cep;
    @Column(name = "endereco")
    private String address;
    @Column(name = "cidade")
    private String city;
    @Column(name = "uf")
    private String uf;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dh_cadastro")
    private Date dateOfCreation;

    public ContatoEntity merge(ContatoEntity contatoEntity) {

        if(Objects.nonNull(contatoEntity.getName())){
            this.setName(contatoEntity.getName());

        }
        if(Objects.nonNull(contatoEntity.getCep())){
            this.setCep(contatoEntity.getCep());

        }
        if(Objects.nonNull(contatoEntity.getTelephone())){
            this.setTelephone(contatoEntity.getTelephone());

        }
        if(Objects.nonNull(contatoEntity.getAddress())){
            this.setAddress(contatoEntity.getAddress());

        }
        if(Objects.nonNull(contatoEntity.getCity())){
            this.setCity(contatoEntity.getCity());

        }
        if(Objects.nonNull(contatoEntity.getUf())){
            this.setUf(contatoEntity.getUf());

        }
        return this;
    }
}
