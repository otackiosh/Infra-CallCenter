package com.claro.DesenvolvimentodeSoftwareInfraCallCenter.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @AllArgsConstructor @NoArgsConstructor
public class ContatoFilterDTO {
    private String fieldName;
    private QueryOperator operator;
    private String value;
}
