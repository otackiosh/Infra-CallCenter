package com.claro.DesenvolvimentodeSoftwareInfraCallCenter.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @AllArgsConstructor @NoArgsConstructor
public class AddressDTO {
    private String code;
    private String state;
    private String city;
    private String district;
    private String address;

    @Override
    public String toString(){
        return this.getAddress() + " - " + this.getDistrict() +
                ", " + this.getCity() + " - " + this.getState() + ", " + this.getCode();
    }
}
