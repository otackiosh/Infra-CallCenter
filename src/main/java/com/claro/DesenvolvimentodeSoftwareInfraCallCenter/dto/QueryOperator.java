package com.claro.DesenvolvimentodeSoftwareInfraCallCenter.dto;

public enum QueryOperator {
    EQUALS, NOT_EQUALS, GREATER_EQUAL_THAN, LESS_EQUAL_THAN, LIKE
}
