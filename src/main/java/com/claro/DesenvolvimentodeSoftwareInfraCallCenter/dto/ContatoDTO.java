package com.claro.DesenvolvimentodeSoftwareInfraCallCenter.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @AllArgsConstructor @NoArgsConstructor
public class ContatoDTO {
    private String email;
    private String name;
    private String telephone;
    private String cep;
}
