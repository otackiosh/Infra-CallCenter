package com.claro.DesenvolvimentodeSoftwareInfraCallCenter.controllers;

import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.dao.ContatoDAO;
import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.dto.ContatoDTO;
import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.dto.ContatoFilterDTO;
import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.useCase.CreateContatoUseCase;
import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.useCase.DeleteContatoUseCase;
import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.useCase.FindContatoUseCase;
import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.useCase.UpdateContatoUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api")
public class ContatoController {

    @Autowired
    private FindContatoUseCase findContatoUseCase;
    @Autowired
    private CreateContatoUseCase createContatoUseCase;
    @Autowired
    private UpdateContatoUseCase updateContatoUseCase;
    @Autowired
    private DeleteContatoUseCase deleteContatoUseCase;

    @GetMapping(path = "/contatos/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Page<ContatoDAO>> listAll(Pageable pageable, @RequestBody List<ContatoFilterDTO> contatoFilterDTO) {
        return new ResponseEntity<>(findContatoUseCase.execute(contatoFilterDTO, pageable), HttpStatus.OK);
    }

    @PostMapping(path = "/contatos/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ContatoDAO> createContato(@RequestBody ContatoDTO createContatoDTO) {
        return new ResponseEntity<>(createContatoUseCase.execute(createContatoDTO), HttpStatus.CREATED);
    }

    @PutMapping(path = "/contatos/{email}/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ContatoDAO> updateContato(@PathVariable String email, @RequestBody ContatoDTO createContatoDTO) {
        return new ResponseEntity<>(updateContatoUseCase.execute(createContatoDTO, email), HttpStatus.OK);
    }

    @DeleteMapping(path = "/contatos/{email}/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteContato(@PathVariable String email) {
        deleteContatoUseCase.execute(email);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
