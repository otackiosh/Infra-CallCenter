package com.claro.DesenvolvimentodeSoftwareInfraCallCenter.useCase;

import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.dao.ContatoDAO;
import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.dto.AddressDTO;
import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.dto.ContatoDTO;
import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.entity.ContatoEntity;
import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.repository.ContatoRepository;
import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.services.GetFullAddressService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;

@Component
public class UpdateContatoUseCase {
    @Autowired
    private ContatoRepository contatoRepository;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private GetFullAddressService getFullAddressService;

    public ContatoDAO execute(ContatoDTO contatoDTO, String email) {
        ContatoEntity contatoEntity = contatoRepository.findByEmail(email).orElseThrow(() -> new EntityNotFoundException());

        AddressDTO addressDTO = getFullAddressService.execute(contatoDTO.getCep());

        ContatoEntity newContatoEntity =
                ContatoEntity.builder()
                        .cep(contatoDTO.getCep())
                        .name(contatoDTO.getName())
                        .telephone(contatoDTO.getTelephone())
                        .address(addressDTO.toString())
                        .city(addressDTO.getCity())
                        .uf(addressDTO.getState())
                        .build();

        contatoEntity.merge(newContatoEntity);
        return modelMapper.map(contatoRepository.save(contatoEntity), ContatoDAO.class);
    }
}
