package com.claro.DesenvolvimentodeSoftwareInfraCallCenter.useCase;

import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.repository.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
public class DeleteContatoUseCase {

    @Autowired
    private ContatoRepository contatoRepository;

    @Transactional
    public void execute(String email) {
        contatoRepository.deleteByEmail(email);
    }
}
