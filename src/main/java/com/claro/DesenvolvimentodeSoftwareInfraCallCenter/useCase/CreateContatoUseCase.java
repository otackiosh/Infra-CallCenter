package com.claro.DesenvolvimentodeSoftwareInfraCallCenter.useCase;

import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.dao.ContatoDAO;
import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.dto.AddressDTO;
import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.dto.ContatoDTO;
import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.entity.ContatoEntity;
import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.repository.ContatoRepository;
import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.services.GetFullAddressService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class CreateContatoUseCase {

    @Autowired
    private ContatoRepository contatoRepository;
    @Autowired
    private GetFullAddressService getFullAddressService;
    @Autowired
    private ModelMapper modelMapper;


    public ContatoDAO execute(ContatoDTO createContatoDTO) {

        AddressDTO addressDTO = getFullAddressService.execute(createContatoDTO.getCep());

        ContatoEntity newContatoEntity =
                ContatoEntity.builder()
                        .cep(createContatoDTO.getCep())
                        .email(createContatoDTO.getEmail())
                        .name(createContatoDTO.getName())
                        .telephone(createContatoDTO.getTelephone())
                        .address(addressDTO.toString())
                        .city(addressDTO.getCity())
                        .uf(addressDTO.getState())
                        .dateOfCreation(new Date())
                        .build();

        return modelMapper.map(contatoRepository.save(newContatoEntity), ContatoDAO.class);

    }
}
