package com.claro.DesenvolvimentodeSoftwareInfraCallCenter.useCase;

import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.dao.ContatoDAO;
import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.dto.ContatoFilterDTO;
import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.entity.ContatoEntity;
import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.repository.ContatoRepository;
import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.repository.ContatoSpecification;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class FindContatoUseCase {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private ContatoSpecification contatoSpecification;

    @Autowired
    private ContatoRepository contatoRepository;

    public Page<ContatoDAO> execute(List<ContatoFilterDTO> filters, Pageable pageable) {
        Page<ContatoEntity> contatoEntities = contatoRepository.findAll(contatoSpecification.getSpecificationsFromFilter(filters), pageable);

        return contatoEntities.map(contatoEntity -> modelMapper.map(contatoEntity, ContatoDAO.class));
    }
}
