package com.claro.DesenvolvimentodeSoftwareInfraCallCenter.services;

import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.dto.AddressDTO;
import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.utils.CepFormater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class GetFullAddressService {

    @Autowired
    private CepFormater cepFormater;

    private RestTemplate restTemplate = new RestTemplate();


    public AddressDTO execute(String cep) {

        return restTemplate.getForEntity("https://cdn.apicep.com/file/apicep/" +
                cepFormater.format(cep) + ".json", AddressDTO.class).getBody();
    }
}
