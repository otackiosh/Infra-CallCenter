package com.claro.DesenvolvimentodeSoftwareInfraCallCenter.utils;


import org.springframework.stereotype.Component;

@Component
public class CepFormater {
    public String format(String cep){
        StringBuilder sb = new StringBuilder(cep);
        sb.insert(5, "-");
        return sb.toString();
    }
}
