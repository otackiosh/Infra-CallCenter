package com.claro.DesenvolvimentodeSoftwareInfraCallCenter.useCase;

import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.dao.ContatoDAO;
import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.dto.AddressDTO;
import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.dto.ContatoDTO;
import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.dto.ContatoFilterDTO;
import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.dto.QueryOperator;
import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.entity.ContatoEntity;
import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.repository.ContatoRepository;
import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.repository.ContatoSpecification;
import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.services.GetFullAddressService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;


@ExtendWith(MockitoExtension.class)
public class ContatoUseCaseTest {

    @InjectMocks
    private CreateContatoUseCase createContatoUseCase;

    @InjectMocks
    private UpdateContatoUseCase updateContatoUseCase;

    @InjectMocks
    private FindContatoUseCase findContatoUseCase;
    @Mock
    private ContatoRepository contatoRepository;
    @Mock
    private GetFullAddressService getFullAddressService;

    @Spy
    private ContatoSpecification contatoSpecification;

    @Spy
    private ModelMapper modelMapper;

    private ContatoEntity newContato;

    @BeforeEach
    void setup() throws Exception{
        newContato = new ContatoEntity(
                "luis@teste.com",
                "Luís Otávio",
                "14991433839",
                "17017050",
                "Rua Doutor Armando Pieroni - Vila Riachuelo, Bauru - SP, 17017-050",
                "Bauru",
                "SP",
                new SimpleDateFormat("dd/MM/yyyy").parse("15/04/1997")
        );
    }


    @Test
    void itShouldCreateContatoAndReturnContatoDAO() {

        given(getFullAddressService.execute("17017050")).willReturn(new AddressDTO("17017050", "SP", "Bauru", "Vila Riachuelo", "Rua Doutor Armando Pieroni"));
        given(contatoRepository.save(any())).willReturn(newContato);

        ContatoDTO newContatoDTO = new ContatoDTO(
                "luis@teste.com",
                "Luís Otávio",
                "14991433839",
                "17017050"
        );

        ContatoDAO contatoDAO = createContatoUseCase.execute(newContatoDTO);

        assertEquals(contatoDAO.getEmail(), "luis@teste.com");
        assertEquals(contatoDAO.getName(), "Luís Otávio");
        assertEquals(contatoDAO.getTelephone(), "14991433839");
        assertEquals(contatoDAO.getCep(), "17017050");
        assertEquals(contatoDAO.getAddress(), "Rua Doutor Armando Pieroni - Vila Riachuelo, Bauru - SP, 17017-050");
        assertEquals(contatoDAO.getCity(), "Bauru");
        assertEquals(contatoDAO.getUf(), "SP");
    }


    @Test
    void itShouldUpdateContatoAndReturnContatoDAO() throws Exception{

        given(getFullAddressService.execute("17017050")).willReturn(new AddressDTO("17017050", "SP", "Bauru", "Vila Riachuelo", "Rua Doutor Armando Pieroni"));
        given(contatoRepository.save(any())).willReturn(newContato);
        given(contatoRepository.findByEmail(any())).willReturn(Optional.of(new ContatoEntity(
                "luis@teste.com",
                "Luís Otávio",
                "14991433839",
                "17017050",
                "Rua Doutor Armando Pieroni - Vila Riachuelo, Bauru - SP, 17017-050",
                "Bauru",
                "SP",
                new SimpleDateFormat("dd/MM/yyyy").parse("15/04/1997"))
        ));

        ContatoDTO newContatoDTO = new ContatoDTO(
                "luis@teste.com",
                "Luís Otávio",
                "14991433839",
                "17017050"
        );

        ContatoDAO contatoDAO = updateContatoUseCase.execute(newContatoDTO, newContatoDTO.getEmail());

        assertEquals(contatoDAO.getEmail(), "luis@teste.com");
        assertEquals(contatoDAO.getName(), "Luís Otávio");
        assertEquals(contatoDAO.getTelephone(), "14991433839");
        assertEquals(contatoDAO.getCep(), "17017050");
        assertEquals(contatoDAO.getAddress(), "Rua Doutor Armando Pieroni - Vila Riachuelo, Bauru - SP, 17017-050");
        assertEquals(contatoDAO.getCity(), "Bauru");
        assertEquals(contatoDAO.getUf(), "SP");
    }

    @Test
    void itShouldFindContatoAndReturnContatoDAO() throws Exception{
        List<ContatoFilterDTO> contatoFilterDTOList = new ArrayList<>();
        contatoFilterDTOList.add(new ContatoFilterDTO("dateOfCreation", QueryOperator.GREATER_EQUAL_THAN, "15/11/2020"));

        given(contatoRepository.findAll(any(), (Pageable) any())).willReturn(
                new PageImpl(List.of(new ContatoEntity(
                        "luis@teste.com",
                        "Luís Otávio",
                        "14991433839",
                        "17017050",
                        "Rua Doutor Armando Pieroni - Vila Riachuelo, Bauru - SP, 17017-050",
                        "Bauru",
                        "SP",
                        new SimpleDateFormat("dd/MM/yyyy").parse("15/04/1997")))));

        Page<ContatoDAO> contatoDAO = findContatoUseCase.execute(contatoFilterDTOList, Pageable.ofSize(2));


        assertEquals(contatoDAO.getContent().get(0).getEmail(), "luis@teste.com");
        assertEquals(contatoDAO.getContent().get(0).getName(), "Luís Otávio");
        assertEquals(contatoDAO.getContent().get(0).getTelephone(), "14991433839");
        assertEquals(contatoDAO.getContent().get(0).getCep(), "17017050");
        assertEquals(contatoDAO.getContent().get(0).getAddress(), "Rua Doutor Armando Pieroni - Vila Riachuelo, Bauru - SP, 17017-050");
        assertEquals(contatoDAO.getContent().get(0).getCity(), "Bauru");
        assertEquals(contatoDAO.getContent().get(0).getUf(), "SP");
    }
}
