package com.claro.DesenvolvimentodeSoftwareInfraCallCenter.Controller;

import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.dao.ContatoDAO;
import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.repository.ContatoRepository;
import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.useCase.CreateContatoUseCase;
import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.useCase.DeleteContatoUseCase;
import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.useCase.FindContatoUseCase;
import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.useCase.UpdateContatoUseCase;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@EnableAutoConfiguration(exclude = DataSourceAutoConfiguration.class)
public class ContatoControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private FindContatoUseCase findContatoUseCase;

    @MockBean
    private DeleteContatoUseCase deleteContatoUseCase;

    @MockBean
    private CreateContatoUseCase createContatoUseCase;

    @MockBean
    private UpdateContatoUseCase updateContatoUseCase;

    @MockBean
    private ContatoRepository repository;
    @Autowired
    private ModelMapper modelMapper;


    @Test
    void itShouldReturnContatos() throws Exception {
        given(findContatoUseCase.execute(any(List.class), any(Pageable.class))).willReturn(
            new PageImpl(List.of(new ContatoDAO(
                            "luis@teste.com",
                            "Luís Otávio",
                            "14991433839",
                            "17017050",
                            "Rua Doutor Armando Pieroni - Vila Riachuelo, Bauru - SP, 17017-050",
                            "Bauru",
                            "SP",
                            "2022-11-17 03:06:15"),
                PageRequest.of(0, 1),
                1
            ))
        );


        mockMvc.perform(get("/api/contatos/?page=0&size=200").content("[]").contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().is(200))
            .andExpect(jsonPath("$.content").isArray())
            .andExpect(jsonPath("$.content[0]").isNotEmpty())
            .andExpect(jsonPath("$.content[0].email").value("luis@teste.com"))
            .andExpect(jsonPath("$.content[0].name").value("Luís Otávio"))
            .andExpect(jsonPath("$.content[0].telephone").value("14991433839"))
            .andExpect(jsonPath("$.content[0].cep").value("17017050"))
            .andExpect(jsonPath("$.content[0].city").value("Bauru"))
            .andExpect(jsonPath("$.content[0].address").value("Rua Doutor Armando Pieroni - Vila Riachuelo, Bauru - SP, 17017-050"))
            .andExpect(jsonPath("$.content[0].dateOfCreation").value("2022-11-17 03:06:15"))
            .andExpect(jsonPath("$.content[0].uf").value("SP"));
    }


    @Test
    void itShouldDeleteContato() throws Exception {
        mockMvc.perform(delete("/api/contatos/luis@teste.com.br/")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content("{\n" +
                                "    \"name\": \"Luis Otavio\",\n" +
                                "    \"telephone\": \"14991433839\",\n" +
                                "    \"cep\": \"17017050\"\n" +
                                "}"))
                .andExpect(status().is(200));
    }

    @Test
    void itShouldUpdateContato() throws Exception {
        given(updateContatoUseCase.execute(any(), eq("luis@teste.com.br"))).willReturn(new ContatoDAO());
                mockMvc.perform(put("/api/contatos/luis@teste.com.br/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "    \"name\": \"Luis Otavio\",\n" +
                                "    \"telephone\": \"14991433839\",\n" +
                                "    \"cep\": \"17017050\"\n" +
                                "}"))
                .andExpect(status().is(200));
    }


    @Test
    void itShouldCreateNewContato() throws Exception {
            mockMvc.perform(post("/api/contatos/")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{\n" +
                            "    \"email\": \"luis@teste.com.br\",\n" +
                            "    \"name\": \"Luis Otavio\",\n" +
                            "    \"telephone\": \"14991433839\",\n" +
                            "    \"cep\": \"17017050\"\n" +
                            "}"))
            .andExpect(status().is(201));
    }
}
