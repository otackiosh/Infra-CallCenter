package com.claro.DesenvolvimentodeSoftwareInfraCallCenter.Respository;

import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.dto.ContatoFilterDTO;
import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.dto.QueryOperator;
import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.entity.ContatoEntity;
import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.repository.ContatoRepository;
import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.repository.ContatoSpecification;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.TestPropertySource;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestPropertySource(properties = {
        "spring.jpa.hibernate.ddl-auto=validate"
})
public class ContatoRepositoryTest {

    @Autowired
    ContatoRepository repository;

    @Spy
    ContatoSpecification contatoSpecification;


    @Test
    void itShouldSaveContato() throws Exception {
        ContatoEntity newContato = new ContatoEntity(
                "otavio@teste.com",
                "Luís Otávio",
                "14991433839",
                "17017050",
                "Rua Doutor Armando Pieroni - Vila Riachuelo, Bauru - SP, 17017-050",
                "Bauru",
                "SP",
                new SimpleDateFormat("dd/MM/yyyy").parse("15/04/1997")
        );

        repository.save(newContato);
        assertNotNull(repository.findByEmail("otavio@teste.com").orElse(null));
    }


    @Test
    void itShouldReturnEmptyList(){
        List<ContatoFilterDTO> filtros = new ArrayList<>();
        filtros.add(
                new ContatoFilterDTO("dateOfCreation", QueryOperator.GREATER_EQUAL_THAN, "15/11/2300")
        );

        assertTrue(repository.findAll(contatoSpecification.getSpecificationsFromFilter(filtros), Pageable.ofSize(10)).getContent().isEmpty());
    }

    @Test
    void itShouldReturnContatoDateOfCreationBetweenDates() throws Exception {

        repository.save(new ContatoEntity(
                "luis@teste.com",
                "Luís Otávio",
                "14991433839",
                "17017050",
                "Rua Doutor Armando Pieroni - Vila Riachuelo, Bauru - SP, 17017-050",
                "Bauru",
                "SP",
                new SimpleDateFormat("dd/MM/yyyy").parse("15/04/1997")
        ));

        repository.save(new ContatoEntity(
                "Olumre@teste.com",
                "Olumre",
                "14991433839",
                "17017050",
                "Rua Doutor Armando Pieroni - Vila Riachuelo, Bauru - SP, 17017-050",
                "Bauru",
                "SP",
                new SimpleDateFormat("dd/MM/yyyy").parse("15/04/1998")
        ));

        repository.save(new ContatoEntity(
                "Loapitu@teste.com",
                "Loapitu",
                "14991433839",
                "17017050",
                "Rua Doutor Armando Pieroni - Vila Riachuelo, Bauru - SP, 17017-050",
                "Bauru",
                "SP",
                new SimpleDateFormat("dd/MM/yyyy").parse("15/04/1999")
        ));

        repository.save(new ContatoEntity(
                "Teleror@teste.com",
                "Teleror",
                "14991433839",
                "17017050",
                "Rua Doutor Armando Pieroni - Vila Riachuelo, Bauru - SP, 17017-050",
                "Bauru",
                "SP",
                new SimpleDateFormat("dd/MM/yyyy").parse("16/03/2001")
        ));

        repository.save(new ContatoEntity(
                "Iskoxol@teste.com",
                "Iskoxol",
                "14991433839",
                "17017050",
                "Rua Doutor Armando Pieroni - Vila Riachuelo, Bauru - SP, 17017-050",
                "Bauru",
                "SP",
                new SimpleDateFormat("dd/MM/yyyy").parse("11/01/1993")
        ));

        repository.save(new ContatoEntity(
                "Xaimoyr@teste.com",
                "Xaimoyr",
                "14991433839",
                "17017050",
                "Rua Doutor Armando Pieroni - Vila Riachuelo, Bauru - SP, 17017-050",
                "Bauru",
                "SP",
                new SimpleDateFormat("dd/MM/yyyy").parse("15/04/2022")
        ));



        List<ContatoFilterDTO> filtros = new ArrayList<>();
        filtros.add(
                new ContatoFilterDTO("dateOfCreation", QueryOperator.GREATER_EQUAL_THAN, "15/11/2000")
        );

        filtros.add(
                new ContatoFilterDTO("dateOfCreation", QueryOperator.LESS_EQUAL_THAN, "15/11/2300")
        );

        assertEquals(repository.findAll(contatoSpecification.getSpecificationsFromFilter(filtros), Pageable.ofSize(10)).getContent().size(), 2);
    }
}
