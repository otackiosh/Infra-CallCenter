package com.claro.DesenvolvimentodeSoftwareInfraCallCenter;

import com.claro.DesenvolvimentodeSoftwareInfraCallCenter.dto.AddressDTO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class ContatoDTOTest {



    @Test
    void itShouldReturnAdressAsString(){
        AddressDTO address = new AddressDTO("17017050", "SP", "Bauru", "Vila Riachuelo", "Rua Doutor Armando Pieroni");
        String result = address.toString();
        assertEquals(result, "Rua Doutor Armando Pieroni - Vila Riachuelo, Bauru - SP, 17017050");
    }
}
