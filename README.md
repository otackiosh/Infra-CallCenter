O arquivo "CallCenter-1.0.war" está pronto para deploy no tomcat.


Rota GET("/api/contatos/") possui Pageable e um filtro dinâmico.<br>
&nbsp;&nbsp;&nbsp;&nbsp;- Possui Pageable, pode colocar tamanho por pagina, numero da pagina e/ou ordenação.<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ex: "/api/contatos/?page=2&size=20&sord=dateOfCreation,asc" irá retornar 20 valores
        da segunda página, em ordem crescente da data de criação do contato.<br>
&nbsp;&nbsp;&nbsp;&nbsp;- Para retornar valores sem filtro o body do request deverá conter um array vazio.<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;body = "[ ]"<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Modelo do filtro:<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"fieldName": >Nome do campo<,<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"operator": >Operador<,<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"value": >Valor da Operação<,<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}<br>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Exemplo do body utilizando um filtro:<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[{<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"fieldName": "dateOfCreation",<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"operator": "GREATER_EQUAL_THAN",<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"value": "15/04/2020"<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;},<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"fieldName": "dateOfCreation",<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"operator": "LESS_EQUAL_THAN",<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"value": "20/04/2020"<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}],<br>
Este filtro irá retornar os contatos criados entre as datas 15/04/2020 e 20/04/2020.

Possíveis operadores do filtro:<br>
&nbsp;&nbsp;&nbsp;&nbsp;- EQUALS<br>
&nbsp;&nbsp;&nbsp;&nbsp;- NOT_EQUALS<br>
&nbsp;&nbsp;&nbsp;&nbsp;- GREATER_EQUAL_THAN<br>
&nbsp;&nbsp;&nbsp;&nbsp;- LESS_EQUAL_THAN<br>
&nbsp;&nbsp;&nbsp;&nbsp;- LIKE<br>
(Filtros GREATER_EQUAL_THAN e LESS_EQUAL_THAN só funcionam para o campo dateOfCreation)

Nome dos campos da entidade que podem ser usados para filtros e ordenação:<br>
&nbsp;&nbsp;&nbsp;&nbsp;- email<br>
&nbsp;&nbsp;&nbsp;&nbsp;- name<br>
&nbsp;&nbsp;&nbsp;&nbsp;- telephone<br>
&nbsp;&nbsp;&nbsp;&nbsp;- cep<br>
&nbsp;&nbsp;&nbsp;&nbsp;- address<br>
&nbsp;&nbsp;&nbsp;&nbsp;- city<br>
&nbsp;&nbsp;&nbsp;&nbsp;- uf<br>
&nbsp;&nbsp;&nbsp;&nbsp;- dateOfCreation<br>

Rota DELETE("/api/contatos/{email}/").<br>
&nbsp;&nbsp;&nbsp;&nbsp;- Rota para deletar o contato cadastrado com o email enviado por parâmetro.<br>

Rota PUT("/api/contatos/{email}/")<br>
&nbsp;&nbsp;&nbsp;&nbsp;- Rota para alterar um contato já existente.<br>
&nbsp;&nbsp;&nbsp;&nbsp;Campos válidos para alteração:<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- name<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- telephone<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- cep<br>

Rota POST("/api/contatos/")<br>
&nbsp;&nbsp;&nbsp;&nbsp;- Rota para criar um novo contato.<br>
&nbsp;&nbsp;&nbsp;&nbsp;Campos necessários para criação:<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- email<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- name<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- telephone<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- cep<br>

